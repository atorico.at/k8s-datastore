package http

import (
	"io"
	"fmt"
	"net/http"
    "k8s-configmap-datastore/pkg/k8s"
)

func getPath(w http.ResponseWriter, r *http.Request) {
	// fmt.Printf("got request:", r.Url.Path, "\n")
	fmt.Printf("got request:", r.URL.Path, "\n")
    body, _ := Get("https://httpbin.org/" + r.URL.Path)
	io.WriteString(w, body)
}
func writeConfigMap(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("got request:", r.URL.Path, "\n")
    body, _ := Get("https://httpbin.org/" + r.URL.Path)
    configMap, saveErr := k8s.SaveConfigMap(body)
    fmt.Println("saveErr:", saveErr)
	io.WriteString(w, configMap.String())
}

func Start()(error) {
	http.HandleFunc("/", getPath)
	http.HandleFunc("/config-map/", writeConfigMap)

	err := http.ListenAndServe(":3333", nil)

    return err
}
