package k8s

import (
	"k8s.io/client-go/rest"
    "context"
	"k8s.io/client-go/kubernetes"
    v1 "k8s.io/api/core/v1"
    metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func SaveConfigMap(body string) (*v1.ConfigMap, error) {
	//var kubeconfig *string
	//if home := homedir.HomeDir(); home != "" {
		//kubeconfig = flag.String("kubeconfig", filepath.Join(home, ".kube", "config"), "(optional) absolute path to the kubeconfig file")
	//} else {

		//kubeconfig = flag.String("kubeconfig", "", "absolute path to the kubeconfig file")
	//}
	//flag.Parse()

	//// use the current context in kubeconfig
	//config, err := clientcmd.BuildConfigFromFlags("", *kubeconfig)
	//if err != nil {
		//panic(err.Error())
	//}
    // creates the in-cluster config
    config, err := rest.InClusterConfig()
    if err != nil {
        panic(err.Error())
    }

    // create the clientset
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		panic(err.Error())
	}

    bodyMap := map[string]string{}
    bodyMap["test"] = body

    configmap := &v1.ConfigMap{
        ObjectMeta: metav1.ObjectMeta{
            Name:      "test-configmap",
            Namespace: "dev",
        },
        Data: bodyMap,
    }
    _, errCreate := clientset.CoreV1().ConfigMaps("dev").Create(context.TODO(), configmap, metav1.CreateOptions{})
    return configmap,errCreate
}
